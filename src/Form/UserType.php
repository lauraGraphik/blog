<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        dump($options);
        $builder
            ->add('email', EmailType::class, [
                "label"=>"Votre courriel",
                "attr"=>[
                    "class"=>"rouge form-control"
                ],
                "help"=>"Votre email vous sert vous connecter à votre compte.",
                "label_attr"=>[
                    "class"=>"rouge"
                ]
            ])
            ->add('password', PasswordType::class,  [
                "label"=>"Votre mot de passe",
                "attr"=>[
                    "class"=>"form-control"
                ]
            ])
            ->add('pseudo', null, [
                "label"=>"Votre pseudo",
                "attr"=>[
                    "class"=>"form-control"
                ],
                "help"=>"Ce pseudo s'affichera pour vos commentaires."
            ])
        ;

        if( $options["isAdmin"] == true ){
            $builder->add('roles', ChoiceType::class, [
                'choices'=>[
                    "Rôle administrateur"=>"ROLE_ADMIN",
                   // "Rôle utilisateur"=>"ROLE_USER"
                ],
                "multiple"=>true,
                "expanded"=>true
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'isAdmin'=>false
        ]);
    }
}
