<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front")
     */
    public function index( ArticleRepository $articleRepo): Response
    {
        $articles = $articleRepo->findAll();

        return $this->render('front/index.html.twig', [
            'controller_name' => 'Coucou Fayçal',
            'articles'=>$articles
        ]);
    }

    /**
     * @Route( "/article/{id}", name="front_article" )
     */
    public function show( Article $article ){
        return $this->render("front/show-article.html.twig", [
            "article"=>$article
        ]);
    }
}
