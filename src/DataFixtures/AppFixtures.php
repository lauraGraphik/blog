<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder=$encoder;
    }

    public function load(ObjectManager $manager)
    {
        $users=[
            ["Laura", "user1@user1.fr", "mdp", "ROLE_USER"],
            ["Mathieu",  "user2@user2.fr", "mdp", "ROLE_USER"],
            ["Toto",  "user3@user3.fr", "mdp", "ROLE_USER"],
            ["Simone","admin1@admin1.fr", "mdp", "ROLE_ADMIN"],
            ["Cachou", "admin2@admin2.fr", "mdp", "ROLE_ADMIN"],
            ["admin",  "admin3@admin3.fr", "mdp", "ROLE_ADMIN"]   
        ];

        foreach( $users as $u ){
            $user = new User();
            $password = $this->encoder->encodePassword($user, $u[2]);
            $user->setPseudo( $u[0])
            ->setEmail( $u[1])
            ->setPassword( $password )
            ->setRoles([ $u[3] ]);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
